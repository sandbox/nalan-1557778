<?php

/**
 * Implementation of hook_views_default_views().
 */
function movie_events_views_default_views() {
  $views = array();

  // Exported view: movie_event_slideshows
  $view = new view;
  $view->name = 'movie_event_slideshows';
  $view->description = 'This is the slideshow for movies event. The first image added in the movie gallery will be used in the slideshow';
  $view->tag = '';
  $view->base_table = 'node';
  $view->core = 6;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'view_node' => array(
      'label' => 'Link',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => 'Read more...',
      'exclude' => 0,
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'relationship' => 'none',
    ),
    'field_movie_review_gallery_fid' => array(
      'id' => 'field_movie_review_gallery_fid',
      'table' => 'node_data_field_movie_review_gallery',
      'field' => 'field_movie_review_gallery_fid',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'ddblock_image_field_example_item' => 'ddblock_image_field_example_item',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('style_plugin', 'slideshow');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'mode' => 'views_slideshow_ddblock',
    'views_slideshow_ddblock' => array(
      'widget' => 'cycle',
      'debug_info' => 'drupal',
      'mapping_wrapper' => array(
        'mappings' => array(
          0 => array(
            'target' => 'node_id',
            'source' => 'nid',
          ),
          1 => array(
            'target' => 'slide_image',
            'source' => 'field_ddblock_if_image_fid',
          ),
          2 => array(
            'target' => 'slide_title',
            'source' => 'title',
          ),
          3 => array(
            'target' => 'slide_text',
            'source' => 'field_ddblock_if_slide_text_value',
          ),
          4 => array(
            'target' => 'slide_read_more',
            'source' => 'view_node',
          ),
          5 => array(
            'target' => 'pager_image',
            'source' => 'field_ddblock_if_image_fid',
          ),
          6 => array(
            'target' => 'pager_text',
            'source' => 'field_ddblock_if_pager_item_text_value',
          ),
          7 => array(
            'target' => '',
            'source' => '',
          ),
          8 => array(
            'target' => '',
            'source' => '',
          ),
          9 => array(
            'target' => '',
            'source' => '',
          ),
          10 => array(
            'target' => '',
            'source' => '',
          ),
          11 => array(
            'target' => '',
            'source' => '',
          ),
          12 => array(
            'target' => '',
            'source' => '',
          ),
          13 => array(
            'target' => '',
            'source' => '',
          ),
          14 => array(
            'target' => '',
            'source' => '',
          ),
          15 => array(
            'target' => '',
            'source' => '',
          ),
        ),
      ),
      'template' => 'default',
      'custom_template' => '',
      'content_container' => array(
        'container' => 'div.slide',
        'overflow' => 0,
      ),
      'settings' => array(
        'fx' => 'fade',
        'speed' => '500',
        'timeout' => '5000',
        'order' => 'none',
        'pause' => 1,
        'next' => 0,
        'pager_toggle' => 0,
        'pager_settings' => array(
          'pager' => 'custom-pager',
          'pager_container' => '.custom-pager-item',
          'nr_of_pager_items' => '5',
          'pager_position' => 'top',
          'pager_event' => 'click',
          'pager_fast' => 1,
          'pager_pause' => 1,
        ),
        'pager2' => 1,
        'pager2_settings' => array(
          'pager2_event' => 'click',
          'pager2_position' => array(
            'pager' => '',
            'slide' => 'slide',
          ),
          'pager2_pager' => array(
            'pager2_pager_prev' => 'prev',
            'pager2_pager_next' => 'next',
            'pager2_pager_hide' => 1,
          ),
          'pager2_slide' => array(
            'pager2_slide_prev' => '',
            'pager2_slide_next' => '',
            'pager2_slide_hide' => 1,
          ),
        ),
        'slide_text' => 1,
        'slide_text_settings' => array(
          'slide_text_container' => 'div.slide-text',
          'slide_text_position' => 'bottom',
          'slide_text_jquery' => 0,
          'slide_jquery' => array(
            'slide_text_before_effect' => 'hide',
            'slide_text_before_speed' => '500',
            'slide_text_after_effect' => 'show',
            'slide_text_after_speed' => '500',
          ),
        ),
        'custom' => array(
          'custom_jquery' => '',
        ),
      ),
    ),
  ));
  $handler = $view->new_display('block', 'Movie event slideshow', 'block_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
    'view_node' => array(
      'label' => 'Link',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'text' => 'Read more...',
      'exclude' => 0,
      'id' => 'view_node',
      'table' => 'node',
      'field' => 'view_node',
      'relationship' => 'none',
    ),
    'field_movie_events_gallery_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 1,
      'label_type' => 'none',
      'format' => 'image_plain',
      'multiple' => array(
        'group' => 1,
        'multiple_number' => '1',
        'multiple_from' => '0',
        'multiple_reversed' => 0,
      ),
      'exclude' => 0,
      'id' => 'field_movie_events_gallery_fid',
      'table' => 'node_data_field_movie_events_gallery',
      'field' => 'field_movie_events_gallery_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'body' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '150',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'movie_events' => 'movie_events',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
  ));
  $handler->override_option('style_options', array(
    'mode' => 'views_slideshow_ddblock',
    'views_slideshow_ddblock-prefix' => '',
    'views_slideshow_ddblock' => array(
      'widget' => 'cycle',
      'debug_info' => 'none',
      'template' => 'default',
      'custom_template' => '',
      'template_submit' => 'get template sizes',
      'template_size_wrapper' => array(
        'template_size' => 'default',
      ),
      'custom_template_size' => '',
      'mapping_wrapper' => array(
        'mappings' => array(
          0 => array(
            'target' => 'node_id',
            'source' => '',
          ),
          1 => array(
            'target' => 'slide_image',
            'source' => 'field_movie_events_gallery_fid',
          ),
          2 => array(
            'target' => 'slide_title',
            'source' => 'title',
          ),
          3 => array(
            'target' => 'slide_text',
            'source' => 'body',
          ),
          4 => array(
            'target' => 'slide_read_more',
            'source' => 'view_node',
          ),
          5 => array(
            'target' => 'pager_image',
            'source' => '',
          ),
          6 => array(
            'target' => 'pager_text',
            'source' => '',
          ),
          7 => array(
            'target' => '',
            'source' => '',
          ),
          8 => array(
            'target' => '',
            'source' => '',
          ),
          9 => array(
            'target' => '',
            'source' => '',
          ),
          10 => array(
            'target' => '',
            'source' => '',
          ),
          11 => array(
            'target' => '',
            'source' => '',
          ),
          12 => array(
            'target' => '',
            'source' => '',
          ),
          13 => array(
            'target' => '',
            'source' => '',
          ),
          14 => array(
            'target' => '',
            'source' => '',
          ),
          15 => array(
            'target' => '',
            'source' => '',
          ),
        ),
      ),
      'content_container' => array(
        'container' => 'div.slide',
        'overflow' => 0,
      ),
      'settings' => array(
        'fx' => 'fade',
        'speed' => '500',
        'timeout' => '5000',
        'order' => 'none',
        'pause' => 1,
        'next' => 0,
        'pager_toggle' => 1,
        'pager_settings' => array(
          'pager' => 'custom-pager',
          'pager_container' => '.custom-pager-item',
          'nr_of_pager_items' => '3',
          'pager_position' => 'left',
          'pager_event' => 'click',
          'pager_fast' => 1,
          'pager_pause' => 1,
        ),
        'pager2' => 1,
        'pager2_settings' => array(
          'pager2_event' => 'click',
          'pager2_position' => array(
            'pager' => 'pager',
            'slide' => 'slide',
          ),
          'pager2_pager' => array(
            'pager2_pager_prev' => 'prev',
            'pager2_pager_next' => 'next',
            'pager2_pager_hide' => 1,
          ),
          'pager2_slide' => array(
            'pager2_slide_prev' => '',
            'pager2_slide_next' => '',
            'pager2_slide_hide' => 1,
          ),
        ),
        'slide_text' => 1,
        'slide_text_settings' => array(
          'slide_text_container' => 'div.slide-text',
          'slide_text_position' => 'bottom',
          'slide_text_jquery' => 1,
          'slide_jquery' => array(
            'slide_text_before_effect' => 'hide',
            'slide_text_before_speed' => '500',
            'slide_text_after_effect' => 'show',
            'slide_text_after_speed' => '500',
          ),
        ),
        'custom' => array(
          'custom_jquery' => '',
        ),
      ),
    ),
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);

  $views[$view->name] = $view;

  // Exported view: movie_events
  $view = new view;
  $view->name = 'movie_events';
  $view->description = 'This is the view for movie events which creates blocks and listing page';
  $view->tag = '';
  $view->base_table = 'node';
  $view->core = 6;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'title' => array(
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler = $view->new_display('block', 'Latest events', 'block_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'movie_events' => 'movie_events',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Latest events');
  $handler->override_option('items_per_page', 5);
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('page', 'Movie Events', 'page_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 1,
        'text' => '<h3>[title]</h3>',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_movie_events_gallery_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'galleryformatter_thumb_default',
      'multiple' => array(
        'group' => 1,
        'multiple_number' => '1',
        'multiple_from' => '0',
        'multiple_reversed' => 0,
      ),
      'exclude' => 0,
      'id' => 'field_movie_events_gallery_fid',
      'table' => 'node_data_field_movie_events_gallery',
      'field' => 'field_movie_events_gallery_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'body' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'absolute' => 0,
        'link_class' => '',
        'alt' => '',
        'rel' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 1,
        'max_length' => '150',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'hide_alter_empty' => 1,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'movie_events' => 'movie_events',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Movie events');
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('path', 'latest-events');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));

  $views[$view->name] = $view;

  return $views;
}
