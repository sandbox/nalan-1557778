<?php

/**
 * Implementation of hook_strongarm().
 */
function movie_review_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_movie_reviews';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_movie_reviews';
  $strongarm->value = '2';
  $export['comment_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_movie_reviews';
  $strongarm->value = '4';
  $export['comment_default_mode_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_movie_reviews';
  $strongarm->value = '1';
  $export['comment_default_order_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_movie_reviews';
  $strongarm->value = '50';
  $export['comment_default_per_page_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_movie_reviews';
  $strongarm->value = '3';
  $export['comment_controls_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_movie_reviews';
  $strongarm->value = 0;
  $export['comment_anonymous_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_movie_reviews';
  $strongarm->value = '1';
  $export['comment_subject_field_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_movie_reviews';
  $strongarm->value = '1';
  $export['comment_preview_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_movie_reviews';
  $strongarm->value = '0';
  $export['comment_form_location_movie_reviews'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_movie_reviews';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '2',
    'revision_information' => '6',
    'author' => '4',
    'options' => '7',
    'comment_settings' => '8',
    'menu' => '5',
    'taxonomy' => '-3',
  );
  $export['content_extra_weights_movie_reviews'] = $strongarm;

  return $export;
}
