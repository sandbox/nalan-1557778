<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function movie_review_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function movie_review_node_info() {
  $items = array(
    'movie_reviews' => array(
      'name' => t('Movie reviews'),
      'module' => 'features',
      'description' => t('This is the content type for movie review nodes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('REVIEW'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function movie_review_views_api() {
  return array(
    'api' => '2',
  );
}
