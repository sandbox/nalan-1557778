This is a drupal 6 distribution for movie reviews.
It contains following features:

*******************Content types*******************

    * Movie reviews
    * Movie events

The following things are done through views
*******************Blocks*******************

    * Latest reviews
    * Latest events

*******************Slideshow*******************

    * Movie review slideshow (with image, short description and title created in movie review content type)
    * Movie eventslideshow (with image, short description and title created in movie event content type)

*******************Listing page*******************

    * Movie reviews (10 lists pers page)
    * Movie events (10 lists pers page)

*******************Theme*******************

An readymade theme called Neelambal based on 960 grid system